// ../server/main.js

var dotenv = require('dotenv').config({path: __dirname + '/.env'});

const express = require('express');
const feathers = require('@feathersjs/feathers');
const socketio = require('@feathersjs/socketio');

const morgan = require('morgan');
const helmet = require('helmet');
const memory = require('feathers-memory');
const path = require('path');
const cors = require('cors');
const bp = require('body-parser');
const cp = require('cookie-parser');

const colors = require('colors');
const errorHandler = require('errorhandler');

var app = express();
app.use(cp());
app.use(bp.urlencoded({ extended: true}));
app.use(bp.json());

app.use(helmet());
app.use(cors());
app.use(morgan('combined'));
// app.configure(socketio());

var router = require('./src/routing/router');

app.use('/api', router);

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({ error : err });
});

// app.set('clientSecret', 'replace_this_with_a_secret_hash');

if ( 'development' === process.env.NODE_ENV ) {
    app.use( errorHandler() );
    dotenv.load();
};

// Add any new connection to the real-time server
    // -- app.on('connection', connection => app.channel('everybody').join(connection));
    // -- app.publish(data => app.channel('everybody'));

var port = process.env.PORT || 3060;
var listener = app.listen(port, function() {
    console.log(colors.yellow.underline('Your server is listening on port ' + listener.address().port + '😁'));
});