'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
   return queryInterface.createTable('users', { 
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        len: [2,32],
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        validate: {
            isEmail: true
        },
      },
      username: {
        type: Sequelize.STRING,
        allowNull: false,
        len: [4,32],
        unique: true,
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false,
        len: [6,64],
      },
      accessToken: {
        type: Sequelize.STRING,
      },
      foursquare_token: {
        type: Sequelize.STRING,
      },
      spotify_token: {
        type: Sequelize.STRING,
      },
      lyft_token: {
        type: Sequelize.STRING,
      },
   });
  },

  down: (queryInterface /* Sequelize */) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
      return queryInterface.dropTable('users');
  },
};
