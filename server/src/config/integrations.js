// import oauth2

/**
 * oAuth works by storing a third party API's token to a respective user schema's hasMany token table and verify it as valid auth for full functionality.
 * Airblocks pulls data from APIs and does stuff in the applets with it: gets token, then establishes connection instance, and does business logic
 * 
 * 
 * Dependencies: Http && HTTPDispatcher && Axios && Websocket && QueryString && EventEmitter && Datastore (Token Entity Table) && OAuth2 && fs
 * 
 * @param {*} code 
 * @param {*} uri 
 */

// https://api.slack.com/methods/oauth.access
function oauth(code, uri) {
  return post({
    url: 'https://slack.com/api/oauth.access',
    transform: JSON.parse,
    form: {
      client_id: process.env.SLACK_CLIENT_ID,
      client_secret: process.env.SLACK_CLIENT_SECRET,
      code: code
    }
  });
}
