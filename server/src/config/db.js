// DB ORM: ../server/src/config/db.js

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const sqlize = require('sequelize');
const config = require('../config/config.json');
const UserModel = require('../entities/user.js'); 

const connection = new sqlize(config.development.database, config.development.username, process.env.POSTGRES_PASSWORD, { 
    host: config.development.host,
    port: config.development.port,
    dialect: 'postgres',
  
    logging: process.env.DB_LOGGING === 'false',

    pool: {
      max: 2,
      min: 1,
      acquire: 120000,
      idle: 120000,
      evict: 120000,
      handleDisconnects: true,
    },

    operatorsAliases: true,
    connectTimeout: 60000,

});

connection
    .authenticate()
    .then( () => {
        console.log('SEQUELIZE: Database connection has been established successfully.');
    })
    .catch( err => {
        console.error('SEQUELIZE: Unable to successfully connect to the database.', err);
    });

// Connect entities to database along with any associations

const User = UserModel(connection, sqlize);

User.prototype.authorize = function(email) {
    var payload = { user: email };
    var token = jwt.sign(payload, process.env.APP_SECRET, {
        expiresIn: 10080 // expires in 7 days
    });
    return token;
};

User.prototype.verifyToken = async function(token) {
    // Verify the user's access before showing the resources
    var isVerified = jwt.verify(token, process.env.APP_SECRET);
    return isVerified;
};

User.prototype.validatePassword = async function(password) {
    return await bcrypt.compare(password, this.password);
}

User.prototype.logout = async function() {
    // logout a user
};

// Applet.belongsTo(User);
// Integrations.belongsToMany(User, {through: '', unique: false})

connection.sync({force: false})
    .then(() => {
        console.log("SEQUELIZE: Database & tables have been created and synced.")
    });

module.exports = {
    User,
};