// REGISTRATION CONTROLLER: ../server/src/controllers/register/registrationcontroller.js

var {User} = require('../../config/db');
// import passport

/**
 * @exports
 * @param {any} req 
 * @param {any} res 
 */

exports.signupGET = function(req, res, next){
    res.json({ "msg" : "Login to Airblocks" })
};

exports.signupPOST = function(req, res, next){

    // REMEMBER: add error checking!!!

    User.create({ name: req.body.name, username: req.body.username, password: req.body.password, email: req.body.email }).then(user => {
        var generatedToken = user.authorize(user.email);
        user.token = generatedToken;
        user.save();
        res.status(201).send({ "msg" : "User successfully created", "user" : user.email, "token" : user.token});
    }).catch(error => res.status(400).send(error));
};

