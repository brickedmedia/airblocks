// INDEX CONTROLLER: ../server/src/controllers/index/indexcontroller.js

var {User} = require('../../config/db');


/**
 * @exports
 * @param {any} req 
 * @param {any} res 
 */

exports.landing = function(req, res, next) {
    // landing page data
    res.setHeader('Content-Type', 'application/json');
    var data = JSON.stringify(
        { 
            'key' : 'value',
            'msg'  : 'hello ya\'ll',
            a  : 1,
        }
    );
    res.send(data);
}

exports.dashboard = function(req, res, next) {
    res.json({"err": "are you logged in?"});
}