// LYFT_CONTROLLER

var lyftAPI = require('lyft-node');
const lyft = new lyftAPI(process.env.LYFT_CLIENT_ID, process.env.LYFT_CLIENT_SECRET);

/**
 * @exports
 * @param {any} req 
 * @param {any} res 
 */

exports.loginGET = async function(req, res, next){
    const query = {
        start: {
            latitude: 1,
            longitude: 2,
          },
    };

    lyft.getRideTypes(query)
    .then((result) => {
        res.status(301).send(result);
    })
    .catch((error) => {
        res.status(500).send(error);
    });
}

exports.loginREDIRECT = async function(req, res, next){
    res.json({ "msg2" : "welcome to login" });
}
