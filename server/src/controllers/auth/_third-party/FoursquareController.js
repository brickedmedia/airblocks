// FOURSQUARE_CONTROLLER

var foursquare = (require('foursquarevenues'))(process.env.FOURSQUARE_CLIENT_ID, process.env.FOURSQUARE_CLIENT_SECRET);

// https://airblocks-aws.herokuapp.com/

/**
 * @exports
 * @param {any} req 
 * @param {any} res 
 */

exports.dataGET = async function(req, res, next){
    var params = {
        "ll": "-41.7, 52.4" //req.body.coordinates
    };
 
    foursquare.getVenues(params, function(error, venues) {

        var errorObj = JSON.parse(error);

        if (!errorObj == null){
            if (errorObj.meta.errorType === "invalid_auth") {
                res.status(500).send({ "error" : "unauthorized_access", "msg" : "You need to sign in, sorry." });
            } 
        }

        if (error) {
            res.status(500).send(error);
        }

        if (!error) {
            res.status(301).send(venues);
        }
    });
}

exports.loginREDIRECT = async function(req, res, next){
    res.json({ "msg2" : "welcome to login" });
}