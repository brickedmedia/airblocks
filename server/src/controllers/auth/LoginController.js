// LOGIN CONTROLLER: ../server/src/controllers/auth/logincontroller.js

    // Add JWT Verification to Tokens

var {User} = require('../../config/db');

/**
 * @exports
 * @param {any} req 
 * @param {any} res 
 */

exports.loginGET = async function(req, res, next){
    res.json({ "msg" : "welcome to login" });
}


exports.loginPOST = function(req, res, next){
    // user has sent data to the login page
    User.findOne({ where: { email: req.body.email }}).then(
        async function(user) {
            if(!user) {
                res.status(500).send({"Error" : "No user found, try again!"});
            } else if (!await user.validatePassword(req.body.password)) {
                res.status(500).send({"Error" : "Your password is invalid, try again!"});
            } else {
                var generatedToken = user.authorize(user.email);
                user.token = generatedToken;
                user.save();
                res.status(200).send({ "msg" : "Successfully authenticated", "user" : user.email, "token" : user.token});
            }
    }).catch(error => 
        res.status(400).send({ "Error" : error })
    );
};