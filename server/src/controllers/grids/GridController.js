// create datagrids

var {Grid} = require('../../config/db');

/**
 * @exports
 * @param {any} req 
 * @param {any} res 
 */

exports.createNEW = function(req, res, next){

    // async function. await query. do works. send status res or data.

    // user inputs {data} to create a grid
    var spreadbase = new Grid();
    res.json({
        "create_new_grid": "uh this is where u create it",
        "grid" : spreadbase,
    });
};
