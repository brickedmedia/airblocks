// USER MODEL: ../server/src/entities/users/user.js

const bcrypt = require('bcrypt');

module.exports = (sequelize, type) => {
    return sequelize.define('user', {
        id: {
          type: type.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false
        },
        name: {
            type: type.STRING,
            allowNull: false,
            len: [2,32],
        },
        email: {
            type: type.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: true
            },
        },
        username: {
            type: type.STRING,
            allowNull: false,
            len: [4,32],
            unique: true,
        },
        password: {
            type: type.STRING,
            allowNull: false,
            len: [6,64],
        },
        accessToken: {
            type: type.STRING,
        },
        foursquare_token: {
            type: type.STRING,
        },
        spotify_token: {
            type: type.STRING,
        },
        lyft_token: {
            type: type.STRING,
        },
    }, {
        paranoid: true,
        /* instanceMethods: {} */
        hooks: {
            // Prehooks
            beforeCreate: async function(user){
                /**
                 * Takes the plain-text password,
                 * hashes it and salts it before
                 * storing it as the new password digest.
                 */
                const salt = await bcrypt.genSalt(10);
                user.password = await bcrypt.hash(user.password, salt);
            },
        },
    });
};

// Validations, REGEX MATCHING, Attributes, Serializations

/**
 * _id
 * name
 * email
 * password_digest
 * created_at
 * updated_at
 */
