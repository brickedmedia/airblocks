// API V1. ROUTER: ../server/src/routing/router.js

const express = require('express');
const colors = require('colors');

var index = require('../controllers/index/IndexController');
var register = require('../controllers/register/RegistrationController');
var auth = require('../controllers/auth/LoginController');

var lyft = require('../controllers/auth/_third-party/LyftController');
var foursquare = require('../controllers/auth/_third-party/FoursquareController');
var spotify = require('../controllers/auth/_third-party/SpotifyController');

var grids = require('../controllers/grids/GridController');
var integrations = require('../controllers/settings/IntegrationsController');

var api = express.Router();

// Middleware
api.use(function(req, res, next) {
    
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:' + 3060);
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Add Token verification middleware by checking headers for token & jwt.verify then next(); or throw error;


        // figure out how to white list certain routes from "auth required" error mw messages
        // ---- E.G.: if req.Authenticated, return next() else redirect UNLESS auth not required
    console.log(colors.yellow.underline("Requests have started..👍"));
    next();
});


api.route('/')
.get(index.landing);

api.route('/signup')
.get(register.signupGET)
.post(register.signupPOST);

api.route('/login')
.get(auth.loginGET)
.post(auth.loginPOST);

api.route('/dashboard')
.get(index.dashboard);

// Third Party Authorizations

/* 
    Callback receives access token, 
    stores it to the user Tokens table 
    and redirects to the client. 
*/

api.route('/auth/lyft')
.get(lyft.loginGET);

api.route('/auth/lyft/callback')
.get(lyft.loginREDIRECT);

api.route('/auth/foursquare')
.get(foursquare.dataGET);

api.route('/auth/foursquare/callback')
.get(foursquare.loginREDIRECT);

api.route('/auth/spotify')
.get(spotify.loginGET);

api.route('/auth/spotify/callback')
.get(spotify.loginREDIRECT);

// List a user's available third-party app connections [based on tokens]

api.route('/integrations/all')
.get(integrations.listAvailable);

// App Logic: Grids

api.route('/new/grid')
.post(grids.createNEW);

module.exports = api;