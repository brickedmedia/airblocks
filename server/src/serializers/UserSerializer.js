// USER SERIALIZER: ../server/src/serializers/userserializer.js

var JSONAPISerializer = require("json-api-serializer");
var UserSerializer = new JSONAPISerializer();

// Register the "User" type

UserSerializer.register("user", {
    key: "value" // or object.reference
}); // type, options


/**
 * JAS Reference: https://www.npmjs.com/package/json-api-serializer
 * Sequelizer Serializing: https://stackoverflow.com/questions/34978056/how-to-serialize-represent-database-objects-as-json-returned-by-api-in-node
 */