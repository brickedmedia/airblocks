# ENV

* Babelified, for ES7 w/ Webpack?

I Need to add Docker compose

* Vue.js | Express, AMQP TaskQueue, Docker Compose.

---

Sequelize can serialize the data before it's returned, like so: > find, > then format.

---

# For the backend, we have a focus on:

 Router <---> Controller --> <-- Model Entities --> Database


 ## Database Schema

 #### User

 id     :   integer
 name   :   string
 email  :   string
 encrypted_password : string
 created_at : date


---

# SECURITY

> Avoid XSS, use cookie auth tokens signage for Vue web client: https://forum.vuejs.org/t/vuejs-jwt/14976/18