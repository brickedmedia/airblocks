// API: ./api.js

// import Vue from 'vue';
// import axios from 'axios';

/* const client = axios.create({
  baseURL: 'http://localhost:3060',
  json: true,
}); * */

export default {
  // method: require the accessToken for each request

  getIndex() {
    return this.execute('get', '/');
  },

  getSignup() {
    return this.execute('get', '/signup');
  },

  createUser(data) {
    return this.execute('post', '/signup', data);
  },
};
