import Vue from 'vue';
import Router from 'vue-router';

import Index from './views/Index.vue';

import Login from './views/_user/Login.vue';
import Signup from './views/_user/Signup.vue';

import EditProfile from './views/_settings/EditProfile.vue';
import EditPassword from './views/_settings/EditPassword.vue';
import Integrations from './views/_settings/Integrations.vue';


import Dashboard from './views/_app/Dashboard.vue';
import Builder from './views/_app/Builder.vue';
import Settings from './views/_app/Settings.vue';
import Logout from './views/_app/Logout.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/app/signup',
      name: 'signup',
      component: Signup,
    },
    {
      path: '/app/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/app/blocks',
      name: 'dashboard',
      component: Dashboard,
    },
    {
      path: '/app/editor',
      name: 'editor',
      component: Builder,
    },
    {
      path: '/app/settings',
      name: 'settings',
      component: Settings,
    },
    {
      // connected accounts :)
      path: '/app/settings/integrations',
      name: 'integrations',
      component: Integrations,
    },
    // user account edits
    {
      path: '/app/settings/profile',
      name: 'profile-edit',
      component: EditProfile,
    },
    {
      path: '/app/settings/password',
      name: 'password-edit',
      component: EditPassword,
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout,
    },
  ],
});
